﻿using System.Collections.Generic;

namespace Interfaces
{
    public interface IDamageZone
    {
        public Dictionary<IDamageable, float> GetHeroesToApplyDamage();
    }
}