using System;
using System.Collections.Generic;
using Interfaces;
using UnityEngine;

[RequireComponent(typeof(Collider2D), typeof(SpriteRenderer))]
public class DamageZone : MonoBehaviour, IDamageZone
{
    [SerializeField] private float _zoneDamage;
    private Collider2D _collider;
    private List<IDamageable> _heroesInsideZone;


    private void Awake()
    {
        _heroesInsideZone = new List<IDamageable>();
        _collider = GetComponent<Collider2D>();
        _collider.isTrigger = true;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.TryGetComponent(out IDamageable hero))
        {
            _heroesInsideZone.Add(hero);
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.TryGetComponent(out IDamageable hero))
        {
            _heroesInsideZone.Remove(hero);
        }
    }

    public Dictionary<IDamageable, float> GetHeroesToApplyDamage()
    {
        Dictionary<IDamageable, float> heroes = new Dictionary<IDamageable, float>();
        foreach (var hero in _heroesInsideZone)
        {
            heroes.Add(hero, _zoneDamage);
        }

        return heroes;
    }
}