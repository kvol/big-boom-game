﻿using System;
using System.Collections.Generic;
using Interfaces;
using TMPro;
using UnityEngine;

namespace Debugging
{
    public class DebugManager : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI _heroesDebug;
        private Hero[] _heroes;
        private readonly List<Rigidbody2D> _heroesRigidbody = new List<Rigidbody2D>();

        private Dictionary<IHeroActions, MovementChecker.State> _heroesStates1 =
            new Dictionary<IHeroActions, MovementChecker.State>();

        private Dictionary<IHeroActions, MovementChecker.State> _heroesStates2 =
            new Dictionary<IHeroActions, MovementChecker.State>();

        private void Start()
        {
            _heroesDebug.gameObject.SetActive(true);
            _heroes = FindObjectsOfType<Hero>();
            _heroesStates1 = FindObjectsOfType<HeroesActionsListener>()[0]._heroesStates;
            _heroesStates2 = FindObjectsOfType<HeroesActionsListener>()[1]._heroesStates;
            foreach (var hero in _heroes)
            {
                _heroesRigidbody.Add(hero.GetComponent<Rigidbody2D>());
            }
        }

        private void FixedUpdate()
        {
            _heroesDebug.text = "";
            // for (var i = _heroesRigidbody.Count - 1; i >= 0; i--)
            // {
            //     if (_heroesRigidbody[i] == null)
            //     {
            //         _heroesRigidbody.RemoveAt(i);
            //         continue;
            //     }
            //
            //     _heroesDebug.text += _heroesRigidbody[i].name + " " + _heroesRigidbody[i].IsAwake() + "\n";
            // }

            foreach (var state in _heroesStates1)
            {
                _heroesDebug.text += ((Hero) state.Key).name + " " + state.Value + "\n";
            }

            _heroesDebug.text += "\n\n";

            foreach (var state in _heroesStates2)
            {
                _heroesDebug.text += ((Hero) state.Key).name + " " + state.Value + "\n";
            }
        }
    }
}