using UnityEngine;
using UnityEngine.UI;

namespace UI.Elements
{
    [RequireComponent(typeof(RectTransform))]
    public class ProgressBar : MonoBehaviour
    {
        //Value from 0 to 1
        [SerializeField] private float _value;
        [SerializeField] private Image _backgroundImage;
        [SerializeField] private Image _progressBarImage;
        [SerializeField] private RectTransform _backgroundRect;
        [SerializeField] private RectTransform _progressBarRect;

        public float Value
        {
            get => _value;
            set
            {
                _value = Mathf.Clamp01(value);
                var scale = _progressBarRect.localScale;
                scale.x = _value;
                _progressBarRect.localScale = scale;
            }
        }

        public void ChangeProgressBarColor(Color mainColor)
        {
            float H, S, V;

            Color.RGBToHSV(mainColor, out H, out S, out V);
            V *= 0.75f;
            Color secondaryColor = Color.HSVToRGB(H, S, V);

            _progressBarImage.color = mainColor;
            _backgroundImage.color = secondaryColor;
        }
    }
}