﻿using DG.Tweening;
using Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Elements
{
    [RequireComponent(typeof(Image)), ExecuteInEditMode]
    public class HeroView : MonoBehaviour
    {
        // [SerializeField, Range(0, 1.0f)] private float _partOfShift;
        private Image _attackPointer;
        private bool _isDead;

        private Image _image;
        private RectTransform _rectTransform;
        private Vector2 _upVector;

        private IHeroView _hero;
        private Material _heroViewMulticoloredMaterial;
        private Material _heroViewWhiteBlackMaterial;

        [SerializeField] private float _baseOffset;
        
        public void Initialize(IHeroView hero, Image attackPointer, Material heroViewMulticoloredMaterial,
            Material heroViewWhiteBlackMaterial, Sprite heroSprite, int offset, Vector2 upVector)
        {
            _image = GetComponent<Image>();
            _rectTransform = GetComponent<RectTransform>();
            _hero = hero;
            _attackPointer = attackPointer;

            _upVector = upVector;
            _heroViewMulticoloredMaterial = heroViewMulticoloredMaterial;
            _heroViewWhiteBlackMaterial = heroViewWhiteBlackMaterial;
            _image.sprite = heroSprite;
            _rectTransform.anchoredPosition =
                new Vector2(_baseOffset + offset * _rectTransform.rect.width * 1.1f,
                    50 * _upVector.y);

            _hero.Died += MakeDead;
            _hero.ReadyToLaunch += Highlight;
            _hero.Launched += RemoveHighlight;
        }

        private void Highlight(IHeroActions hero)
        {
            var localPosition = _attackPointer.transform.localPosition;
            localPosition.x = transform.localPosition.x + _rectTransform.rect.width / 2;
            _attackPointer.transform.localPosition = localPosition;
            _attackPointer.gameObject.SetActive(true);
            // _rectTransform.DOLocalMove(
            //     _rectTransform.localPosition + (Vector3) (_upVector * _partOfShift * _rectTransform.rect.height), 0.5f);
        }

        private void RemoveHighlight(IHeroActions hero)
        {
            // _rectTransform.DOLocalMove(
            //     _rectTransform.localPosition - (Vector3) (_upVector * _partOfShift * _rectTransform.rect.height),
            //     0.5f);
        }

        [ContextMenu("Make dead")]
        public void MakeDead(IHeroActions hero, Vector3 position)
        {
            _isDead = true;
            _image.material = _heroViewWhiteBlackMaterial;
        }
    }
}