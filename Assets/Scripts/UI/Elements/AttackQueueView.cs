﻿using System.Collections.Generic;
using Interfaces;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Elements
{
    public class AttackQueueView : MonoBehaviour
    {
        [SerializeField] private HeroView _heroViewPrefab;
        [SerializeField] private Vector2 _upVector;
        [SerializeField] private Image _attackPointer;


        private readonly Queue<HeroView> _aliveHeroes = new Queue<HeroView>();


        public void Initialize(Queue<IHeroView> heroes, Material heroViewMulticoloredMaterial,
            Material heroViewWhiteBlackMaterial)
        {
            var offset = 0;
            foreach (var vHero in heroes)
            {
                var instantiatedHeroView = Instantiate(_heroViewPrefab, transform);
                _aliveHeroes.Enqueue(instantiatedHeroView);
                instantiatedHeroView.Initialize(vHero, _attackPointer, heroViewMulticoloredMaterial,
                    heroViewWhiteBlackMaterial,
                    vHero.GetHeroViewSprite(), offset++, _upVector);
            }
        }
    }
}