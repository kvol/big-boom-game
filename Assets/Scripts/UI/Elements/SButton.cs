﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Elements
{
    public class SButton : MonoBehaviour
    {
        public event Action Clicked;
        private Button _button;

        private void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(() => Clicked?.Invoke());
        }
    }
}