﻿using UI.Window;
using UnityEngine;

namespace UI
{
    public abstract class WindowBase : MonoBehaviour
    {
        public void Open()
        {
            gameObject.SetActive(true);
            SubscribeAllActions();
        }

        public void Close()
        {
            gameObject.SetActive(false);
            UnsubscribeAllActions();
        }

        public abstract void SubscribeAllActions();

        public abstract void UnsubscribeAllActions();

    }
}