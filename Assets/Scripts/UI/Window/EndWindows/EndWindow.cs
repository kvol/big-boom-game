﻿using System;
using UI.Elements;
using UnityEngine;
using Utilities.Enums;

namespace UI.Window.EndWindows
{
    public abstract class EndWindow : WindowBase
    {
        [SerializeField] private SButton _mainMenuButton;
        [SerializeField] private SButton _restartButton;

        protected Action<LoadSceneType> ChangeSceneCallback;

        public void Initialize(Action<LoadSceneType> changeSceneCallback)
        {
            ChangeSceneCallback = changeSceneCallback;
        }

        public override void SubscribeAllActions()
        {
            _mainMenuButton.Clicked += OnMainMenuButtonClicked;
            _restartButton.Clicked += OnRestartButtonClicked;
        }


        public override void UnsubscribeAllActions()
        {
            _mainMenuButton.Clicked -= OnMainMenuButtonClicked;
            _restartButton.Clicked -= OnRestartButtonClicked;
        }


        private void OnMainMenuButtonClicked()
        {
            ChangeSceneCallback(LoadSceneType.MainMenu);
        }

        private void OnRestartButtonClicked()
        {
            ChangeSceneCallback(LoadSceneType.Restart);
        }
    }
}