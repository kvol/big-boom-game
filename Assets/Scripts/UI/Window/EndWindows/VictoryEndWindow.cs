﻿using UI.Elements;
using UnityEngine;
using Utilities.Enums;

namespace UI.Window.EndWindows
{
    public class VictoryEndWindow : EndWindow
    {
        [SerializeField] private SButton _nextLevelButton;

        public override void SubscribeAllActions()
        {
            base.SubscribeAllActions();
            _nextLevelButton.Clicked += OnNextLevelClicked;
        }

        public override void UnsubscribeAllActions()
        {
            base.UnsubscribeAllActions();
            _nextLevelButton.Clicked -= OnNextLevelClicked;
        }

        private void OnNextLevelClicked()
        {
            ChangeSceneCallback(LoadSceneType.NextLevel);
        }
    }
}