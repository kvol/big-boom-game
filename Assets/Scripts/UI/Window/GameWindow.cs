﻿using System;
using System.Collections.Generic;
using Interfaces;
using UI.Elements;
using UnityEngine;

namespace UI.Window
{
    public class GameWindow : WindowBase
    {
        [Header("Children of window"), SerializeField]
        private AttackQueueView _attackQueueBlue;

        [SerializeField] private AttackQueueView _attackQueueRed;

        [Header("Materials for Hero View"), SerializeField]
        private Material _heroViewMulticoloredMaterial;

        [SerializeField] private Material _heroViewWhiteBlackMaterial;


        public void Initialize(List<Queue<IHeroView>> heroesQueues)
        {
            _attackQueueBlue.Initialize(heroesQueues[(int)Hero.FightingSide.Blue], _heroViewMulticoloredMaterial, _heroViewWhiteBlackMaterial);
            _attackQueueRed.Initialize(heroesQueues[(int)Hero.FightingSide.Red], _heroViewMulticoloredMaterial, _heroViewWhiteBlackMaterial);
        }
        

        public override void SubscribeAllActions()
        {
        }

        public override void UnsubscribeAllActions()
        {
        }
    }
}