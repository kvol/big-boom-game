﻿using System;
using System.Collections.Generic;
using Interfaces;
using UI.Window;
using UI.Window.EndWindows;
using UnityEngine;
using Utilities.Enums;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private List<WindowBase> _windowBaseList = new List<WindowBase>();

        private readonly Dictionary<WindowType, WindowBase> _currentOpenedWindows =
            new Dictionary<WindowType, WindowBase>();

        private bool _playerHasControl = true;

        public void Initialize(List<Queue<IHeroView>> heroesQueues, Action<LoadSceneType> changeSceneCallback)
        {
            ((GameWindow) _windowBaseList[(int) WindowType.Game]).Initialize(heroesQueues);
            ((VictoryEndWindow) _windowBaseList[(int) WindowType.Victory]).Initialize(changeSceneCallback);
            ((LoseEndWindow) _windowBaseList[(int) WindowType.Lose]).Initialize(changeSceneCallback);
        }

        public void OpenWindow(WindowType windowType)
        {
            _currentOpenedWindows.Add(windowType, _windowBaseList[(int) windowType]);
            _windowBaseList[(int) windowType].Open();
        }

        public void CloseWindow(WindowType windowType)
        {
            _currentOpenedWindows[windowType]?.Close();
            _currentOpenedWindows.Remove(windowType);
        }
    }

    public enum WindowType
    {
        Game,
        Victory,
        Lose,
    }
}