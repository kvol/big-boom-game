using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class TextAnimation : MonoBehaviour
{
    private Transform _transform;
    private Tweener _tweener;

    private void Awake()
    {
        _transform = transform;
        _tweener = _transform.DOPunchScale(new Vector3(0.75f, 0.75f, 0), 1.5f, 0, 0.5f).SetLoops(-1);
    }

    private void OnDestroy()
    {
        _tweener.Kill();
    }
}