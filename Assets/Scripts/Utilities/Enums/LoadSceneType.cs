﻿namespace Utilities.Enums
{
    public enum LoadSceneType
    {
        NextLevel,
        MainMenu,
        Restart,
        CurrentLevel
    }
}