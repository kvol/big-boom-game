﻿using UnityEngine;

namespace Utilities
{
    public abstract class DontDestroy<T> : MonoBehaviour where T : MonoBehaviour
    {
        protected void Awake()
        {
            T[] objs = FindObjectsOfType<T>();

            if (objs.Length > 1)
            {
                Destroy(gameObject);
            }

            DontDestroyOnLoad(gameObject);
        }
    }
}