using UnityEngine;
using UnityEngine.UI;
using Utilities.Enums;

namespace Utilities
{
    [RequireComponent(typeof(Button))]
    public class PlayButton : MonoBehaviour
    {
        private void Start()
        {
            GetComponent<Button>().onClick
                .AddListener(() => FindObjectOfType<SceneController>().ChangeScene(LoadSceneType.CurrentLevel));
        }
    }
}