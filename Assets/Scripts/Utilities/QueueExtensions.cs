﻿using System;
using System.Collections.Generic;

namespace Utilities
{
    public static class QueueExtensions
    {
        public static T ReEnqueue<T>(this Queue<T> queue)
        {
            var t = queue.Dequeue();
            queue.Enqueue(t);
            return t;
        }

        public static T ReEnqueue<T>(this Queue<T> queue, Func<T, bool> stateToEnqueue)
        {
            int initialCount = queue.Count;
            for (int i = 0; i < initialCount; i++)
            {
                T item = queue.Dequeue();
                if (stateToEnqueue(item))
                {
                    queue.Enqueue(item);
                }
            }

            T tDequeue = queue.Dequeue();
            queue.Enqueue(tDequeue);
            return tDequeue;
        }
    }
}