﻿using UnityEngine;

namespace UI
{
    public class Selection : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer _selectionCircle;
        
        public void SetColor(Color color)
        {
            _selectionCircle.color = color;
        }
    }
}