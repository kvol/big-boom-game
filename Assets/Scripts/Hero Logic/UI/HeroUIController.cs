using Interfaces;
using UI;
using UI.Elements;
using UnityEngine;

public class HeroUIController : MonoBehaviour
{
    private IHeroActions _hero;
    [SerializeField] private ProgressBar _healthBar;

    private void Start()
    {
        _hero = GetComponentInParent<IHeroActions>();

        _hero.Damaged += OnHeroDamaged;
    }

    public void ChangeUIColor(Color color)
    {
        _healthBar.ChangeProgressBarColor(color);
    }

    private void OnHeroDamaged(float damageAmount, float maxHealth)
    {
        _healthBar.Value -= damageAmount / maxHealth;
    }

    private void OnDestroy()
    {
        _hero.Damaged -= OnHeroDamaged;
    }
}