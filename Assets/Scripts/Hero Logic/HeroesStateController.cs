﻿using System.Collections.Generic;
using Interfaces;

public class HeroesStateController
{
    private List<List<IStateChangeable>> _heroes;

    public HeroesStateController(List<List<Hero>> spawnedHeroes)
    {
        _heroes = new List<List<IStateChangeable>>();
        foreach (var sideHeroes in spawnedHeroes)
        {
            _heroes.Add(new List<IStateChangeable>());
            foreach (var hero in sideHeroes)
            {
                _heroes[(int) hero.Side].Add(hero);
            }
        }
    }

    public void SetSideHeroesAsAttackers(Hero.FightingSide side)
    {
        foreach (var hero in _heroes[(int) side])
        {
            hero?.SetAsAttacker();
        }
    }

    public void SetSideHeroesAsDefenders(Hero.FightingSide side)
    {
        foreach (var hero in _heroes[(int) side])
        {
            hero?.SetAsDefender();
        }
    }
}