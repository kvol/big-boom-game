﻿namespace Interfaces
{
    public interface IAttackable : IBuffable
    {
        public float ReceivedDamageMultiplier { get; set; }

        public Hero.FightingSide Side { get; set; }
    }
}