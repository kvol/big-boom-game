﻿namespace Interfaces
{
    public interface IHeroStats
    {
        public float Mass { get; set; }

        public float CurrentSpeed { get; set; }

        public float MAXHealth { get; set; }

        public float CurrentHealth { get; set; }

        public float BaseSpeed { get; set; }

        public float BaseDamage { get; set; }

        public float HeroRadius { get; set; }
        public float ReceivedDamageMultiplier { get; set; }
    }
}