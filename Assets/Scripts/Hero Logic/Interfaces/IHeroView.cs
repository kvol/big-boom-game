﻿using System;
using UnityEngine;

namespace Interfaces
{
    public interface IHeroView
    {
        public Sprite GetHeroViewSprite();
        public event Action<IHeroActions, Vector3> Died;
        public event Action<IHeroActions> ReadyToLaunch;
        public event Action<IHeroActions> Launched;
    }
}