﻿using System;
using UnityEngine;

namespace Interfaces
{
    public interface IHeroActions
    {
        public event Action<float, float> Damaged;
        public event Action<IHeroActions> Launched;
        public event Action<IHeroActions, Vector3> Died;
        public event Action<IHeroActions, MovementChecker.State> ChangedMovingState;
        public event Action<IHeroActions> ReadyToLaunch;
        public event Action<IHeroActions> HeroAttackedSomeone;
    }
}