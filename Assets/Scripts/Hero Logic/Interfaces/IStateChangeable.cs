﻿namespace Interfaces
{
    public interface IStateChangeable
    {
        public void SetAsAttacker();
        public void SetAsDefender();
    }
}