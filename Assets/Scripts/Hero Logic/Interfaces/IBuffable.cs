﻿using Buffs;

namespace Interfaces
{
    public interface IBuffable
    {
        public void AddBuff(Buff buffToAdd);
    }
}