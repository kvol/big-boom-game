﻿namespace Interfaces
{
    public interface IDamageable
    {
        void ApplyDamage(float damage);
    }
}