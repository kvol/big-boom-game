using System;
using System.Collections.Generic;
using System.Linq;
using Abilities;
using Buffs;
using DG.Tweening;
using Inputs;
using Interfaces;
using UI;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(SpriteRenderer), typeof(AimController), typeof(HeroLauncher))]
[RequireComponent(typeof(HeroAttacker))]
public class Hero : MonoBehaviour, IInputHandler, IAttackable, IDamageable, IHeroActions, IStateChangeable, IHeroView,
    IHeroStats
{
    [SerializeField] private HeroStats _stats;

    public event Action<float, float> Damaged;
    public event Action<IHeroActions> Launched;
    public event Action<IHeroActions, Vector3> Died;
    public event Action<IHeroActions, MovementChecker.State> ChangedMovingState;
    public event Action<IHeroActions> ReadyToLaunch;
    public event Action<IHeroActions> HeroAttackedSomeone;

    private List<Buff> _buffs = new List<Buff>();

    private HeroLauncher _heroLauncher;
    private HeroAttacker _heroAttacker;
    private HeroUIController _heroUI;
    private MovementChecker _movementChecker;
    private HeroAnimationController _heroAnimationController;

    [SerializeField] private Selection _selection;


    private bool _isLaunchable;

    private float _maxHealth;
    private float _currentHealth;
    private float _baseSpeed;
    private float _currentSpeed;
    private float _baseDamage;
    private float _mass;
    private float _heroRadius;
    private float _receivedDamageMultiplier;

    private Sprite _heroSprite;
    private FightingSide _side;

    public HeroStats Stats => _stats;

    public float ReceivedDamageMultiplier
    {
        get => _receivedDamageMultiplier;
        set => _receivedDamageMultiplier = value;
    }

    public FightingSide Side
    {
        get => _side;
        set
        {
            //TODO:Вынести потом в метод change hero color(или даже в класс)
            _heroUI.ChangeUIColor(value.Equals(FightingSide.Blue) ? Color.blue : Color.red);
            _selection.SetColor(value.Equals(FightingSide.Blue) ? Color.blue : Color.red);
            _side = value;
        }
    }

    public bool IsLaunchable
    {
        get => _isLaunchable;
        set
        {
            _isLaunchable = value;
            _selection.gameObject.SetActive(value);
            if (_isLaunchable)
            {
                ReadyToLaunch?.Invoke(this);
            }
            else
            {
                _aimController.Disable();
            }
        }
    }

    public float Mass
    {
        get => _mass;
        set
        {
            _mass = value;
            GetComponent<Rigidbody2D>().mass = _mass;
        }
    }

    public float CurrentSpeed
    {
        get => _currentSpeed;
        set
        {
            _currentSpeed = value;
            _heroLauncher.LaunchingSpeed = _currentSpeed;
        }
    }

    public float MAXHealth
    {
        get => _maxHealth;
        set => _maxHealth = value;
    }

    public float CurrentHealth
    {
        get => _currentHealth;
        set => _currentHealth = value;
    }

    public float BaseSpeed
    {
        get => _baseSpeed;
        set => _baseSpeed = value;
    }

    public float BaseDamage
    {
        get => _baseDamage;
        set => _baseDamage = value;
    }

    public float HeroRadius
    {
        get => _heroRadius;
        set => _heroRadius = value;
    }

    private AimController _aimController;

    private SpriteRenderer _spriteRenderer;


    private void Awake()
    {
        _heroLauncher = GetComponent<HeroLauncher>();
        _heroAttacker = GetComponent<HeroAttacker>();
        _movementChecker = GetComponent<MovementChecker>();
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _aimController = GetComponent<AimController>();
        _heroUI = GetComponentInChildren<HeroUIController>();
        _heroAnimationController = GetComponent<HeroAnimationController>();
        InitializeHeroStats();
    }

    private void Start()
    {
        _heroAttacker.Initialize(_baseDamage, _side, _stats.HeroAbility);
        _heroAnimationController.Initialization(this);
        SetAsDefender();
        _heroLauncher.HeroLaunched += OnHeroLaunched;
        _movementChecker.StateChanged += OnMovingStateChanged;
        _heroAttacker.HeroAttacked += OnHeroAttacked;
    }

    private void OnHeroAttacked()
    {
        HeroAttackedSomeone?.Invoke(this);
    }

    private void OnMovingStateChanged(MovementChecker movementChecker, MovementChecker.State state)
    {
        ChangedMovingState?.Invoke(this, state);
    }

    private void OnHeroLaunched()
    {
        Launched?.Invoke(this);
        _heroAttacker.IsLaunched = true;
        _movementChecker.StateChanged += WaitUntilSlept;
    }

    private void InitializeHeroStats()
    {
        _currentHealth = _maxHealth = _stats.BaseHealth;
        _baseSpeed = CurrentSpeed = _stats.BaseSpeed;
        _baseDamage = _stats.BaseDamage;
        Mass = _stats.Mass;
        _heroRadius = _stats.Radius;
        _heroSprite = _stats.HeroSprite;
        if (_heroSprite != null)
        {
            _spriteRenderer.sprite = _heroSprite;
        }
    }

    public void ApplyDamage(float damageAmount)
    {
        float realDamageAmount = damageAmount * ReceivedDamageMultiplier;
        ReceivedDamageMultiplier = 0;
        _currentHealth -= realDamageAmount;
        var damageVFX = Instantiate(_stats.DamageParticles, transform.position, transform.rotation);
        DOVirtual.DelayedCall(damageVFX.main.duration, () => Destroy(damageVFX.gameObject));
        Damaged?.Invoke(realDamageAmount, _maxHealth);
        CheckDeath();
    }

    private void CheckDeath()
    {
        if (_currentHealth <= 0)
        {
            Died?.Invoke(this, transform.position);
            Destroy(gameObject);
        }
    }

    public void OnBeginInput(Vector2 inputPoint)
    {
        if (_isLaunchable)
        {
            _aimController.StartAiming();
        }
    }

    public void OnInput(Vector2 inputPoint)
    {
        if (_isLaunchable)
        {
            _aimController.DragAim(inputPoint);
        }
    }

    public void OnEndInput(Vector2 inputPoint)
    {
        if (_isLaunchable)
        {
            _aimController.EndAiming();
        }
    }


    public void SetAsAttacker()
    {
        _heroAttacker.IsAttacker = true;
    }

    public void SetAsDefender()
    {
        _heroAttacker.IsAttacker = false;
        _heroAttacker.IsLaunched = false;
    }

    public Sprite GetHeroViewSprite()
    {
        return _stats.HeroViewSprite;
    }

    public void AddBuff(Buff buffToAdd)
    {
        if (_buffs.Any(buff => buff.GetType() == buffToAdd.GetType()))
        {
            return;
        }

        buffToAdd.Apply(this);

        _buffs.Add(buffToAdd);
        _buffs.RemoveAll(buff => buff.IsExpired);
    }

    private void WaitUntilSlept(MovementChecker movementChecker, MovementChecker.State state)
    {
        if (state == MovementChecker.State.Sleep)
        {
            _buffs.RemoveAll(buff =>
            {
                buff.Tick();
                return buff.IsExpired;
            });
        }

        movementChecker.StateChanged -= WaitUntilSlept;
    }

    public enum FightingSide
    {
        Blue = 0,
        Red = 1
    }
}