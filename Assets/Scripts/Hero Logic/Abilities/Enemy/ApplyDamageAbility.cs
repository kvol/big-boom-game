﻿using Buffs;
using UnityEngine;

namespace Abilities.Enemy
{
    [CreateAssetMenu(fileName = "Apply Damage", menuName = "ScriptableObjects/Abilities/Apply Damage", order = 1)]
    public class ApplyDamageAbility : EnemyHeroAbility
    {
        [SerializeField, Range(0, 1000.0f)] private float _damageReduceAmount;
        [SerializeField, Range(0, 1000.0f)] private float _zoneDamage;

        protected override void Initialize()
        {
            Buff = new ApplyDamageBuff(_damageReduceAmount, _zoneDamage);
        }
    }
}