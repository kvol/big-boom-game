﻿using Buffs;
using UnityEngine;

namespace Abilities.Enemy
{
    [CreateAssetMenu(fileName = "SpeedDown", menuName = "ScriptableObjects/Abilities/SpeedDown", order = 1)]
    public class SpeedDownAbility : EnemyHeroAbility
    {
        [SerializeField, Range(0, 1000.0f)] private float _massIncreasePercent;

        protected override void Initialize()
        {
            Buff = new ReduceSpeedBuff(_massIncreasePercent, TicksToRemoveBuff);
        }
    }
}