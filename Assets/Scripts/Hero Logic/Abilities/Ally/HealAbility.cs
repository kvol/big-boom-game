﻿using System;
using Buffs;
using UnityEngine;

namespace Abilities.Ally
{
    [CreateAssetMenu(fileName = "HealAbility", menuName = "ScriptableObjects/Abilities/Heal", order = 1)]
    public class HealAbility : AllyHeroAbility
    {
        [SerializeField, Range(0, 1000.0f)] private float _healAmount;

        protected override void Initialize()
        {
            Buff = new HealBuff(_healAmount);
        }
        
    }
}