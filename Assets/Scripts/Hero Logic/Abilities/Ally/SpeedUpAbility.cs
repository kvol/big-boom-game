﻿using System;
using Buffs;
using Interfaces;
using UnityEngine;

namespace Abilities.Ally
{
    [CreateAssetMenu(fileName = "SpeedUp", menuName = "ScriptableObjects/Abilities/SpeedUp", order = 1)]
    public class SpeedUpAbility : AllyHeroAbility
    {
        [SerializeField, Range(0, 99.0f)] private float _massDecreasePercent;

        protected override void Initialize()
        {
            Buff = new IncreaseSpeedBuff(_massDecreasePercent, TicksToRemoveBuff);
        }
    }
}