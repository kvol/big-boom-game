﻿using System;
using Buffs;
using Interfaces;
using UnityEngine;

namespace Abilities
{
    public abstract class HeroAbility : ScriptableObject
    {
        protected Buff Buff;
        [SerializeField, Range(0, 100)] private int _ticksToRemoveBuff;

        public int TicksToRemoveBuff => _ticksToRemoveBuff;

        public void Apply(IBuffable heroToApply)
        {
            Initialize();
            heroToApply.AddBuff(Buff);
        }

        protected abstract void Initialize();

        private void OnValidate()
        {
            Initialize();
        }
    }
}