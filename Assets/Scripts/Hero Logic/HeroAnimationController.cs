﻿using Interfaces;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class HeroAnimationController : MonoBehaviour
{
    private IHeroActions _heroActions;
    private Animator _animator;
    private static readonly int Stopped = Animator.StringToHash("Stopped");
    private static readonly int AttackedSomeone = Animator.StringToHash("AttackedSomeone");
    private static readonly int Launched = Animator.StringToHash("Launched");

    public void Initialization(IHeroActions heroActions)
    {
        _animator = GetComponent<Animator>();
        _heroActions = heroActions;
        _heroActions.Launched += OnHeroLaunched;
        _heroActions.HeroAttackedSomeone += OnHeroAttackedSomeone;
        _heroActions.ChangedMovingState += OnHeroChangedMovingState;
    }

    private void OnHeroChangedMovingState(IHeroActions hero, MovementChecker.State state)
    {
        _animator.SetBool(Stopped, state == MovementChecker.State.Sleep);
        _animator.SetBool(Launched, state == MovementChecker.State.Awake);
    }

    private void OnHeroAttackedSomeone(IHeroActions obj)
    {
        _animator.SetBool(AttackedSomeone, true);
        _animator.SetBool(AttackedSomeone, false);
    }

    private void OnHeroLaunched(IHeroActions hero)
    {
        _animator.SetBool(Launched, true);
        _animator.SetBool(Launched, false);
    }
}