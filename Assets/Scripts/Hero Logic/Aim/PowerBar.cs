﻿using System;
using UnityEngine;

public class PowerBar : MonoBehaviour
{
    private LineRenderer _powerBar;
    // [SerializeField, Range(0.1f, 10)] private float _maxLenghtOfDrag;

    public const float MaxLenghtOfDrag = 2;


    private void Awake()
    {
        _powerBar = GetComponent<LineRenderer>();
    }

    public void Enable()
    {
        gameObject.SetActive(true);
        SetPowerBar(0);
    }

    private void SetPowerBar(float powerValue)
    {
        var colorGradient = _powerBar.colorGradient;
        var alphaKeys = colorGradient.alphaKeys;

        alphaKeys[2].time = alphaKeys[1].time = powerValue;
        alphaKeys[0].alpha = 1;


        if (powerValue == 0.0f)
        {
            alphaKeys[0].alpha = 0;
        }
        else
        {
            alphaKeys[2].time += 0.1f * powerValue;
        }

        colorGradient.alphaKeys = alphaKeys;
        _powerBar.colorGradient = colorGradient;
    }

    public void UpdatePowerBar(float currentLaunchPower)
    {
        SetPowerBar(currentLaunchPower);
    }


    public void Disable()
    {
        gameObject.SetActive(false);
    }
}