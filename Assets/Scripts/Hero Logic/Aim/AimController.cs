using UnityEngine;


[RequireComponent(typeof(HeroLauncher))]
public class AimController : MonoBehaviour
{
    [SerializeField] private AimLine _aimLine;
    [SerializeField] private PowerBar _powerBar;
    private HeroLauncher _heroLauncher;

    private float _launchPowerModifier;
    private Vector2 _aimDirection;

    private Camera _mainCamera;

    private void Awake()
    {
        _mainCamera = Camera.main;
        _heroLauncher = GetComponent<HeroLauncher>();
    }

    public void StartAiming()
    {
        Enable();
    }


    public void EndAiming()
    {
        Disable();
        _heroLauncher.Launch(_aimDirection, _launchPowerModifier);
    }


    public void DragAim(Vector2 aimDirection)
    {
        _aimDirection = aimDirection;

        _launchPowerModifier = Mathf.Clamp(_aimDirection.magnitude, 0, PowerBar.MaxLenghtOfDrag) /
                               PowerBar.MaxLenghtOfDrag;


        _aimLine.UpdateAimLine(_aimDirection);
        _powerBar.UpdatePowerBar(_launchPowerModifier);
    }

    private void Enable()
    {
        _aimLine.Enable();
        _powerBar.Enable();
    }

    public void Disable()
    {
        _aimLine.Disable();
        _powerBar.Disable();
    }
}