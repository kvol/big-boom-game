﻿using System;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class AimLine : MonoBehaviour
{
    [SerializeField, Range(0.0f, 10.0f)] private float _maxAimLineLenght;

    private LineRenderer _aimLine;

    private void Awake()
    {
        _aimLine = GetComponent<LineRenderer>();
        _aimLine.widthMultiplier = transform.lossyScale.x;
        CheckPointsOfLine();
    }

    private void CheckPointsOfLine()
    {
        if (_aimLine.positionCount != 2)
        {
            throw new UnityException("Wrong points count!");
        }
    }

    public void Enable()
    {
        gameObject.SetActive(true);
    }

    public void Disable()
    {
        gameObject.SetActive(false);
    }

    public void UpdateAimLine(Vector2 aimDirection)
    {
        _aimLine.SetPosition(1, aimDirection.normalized * _maxAimLineLenght);
    }
}