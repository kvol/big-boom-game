using Abilities;
using UnityEngine;

[CreateAssetMenu(fileName = "Hero", menuName = "ScriptableObjects/Add new hero stat", order = 1)]
public class HeroStats : ScriptableObject
{
    [SerializeField] private float _baseHealth;
    [SerializeField] private float _baseSpeed;
    [SerializeField] private float _baseDamage;
    [SerializeField] private float _mass;
    [SerializeField] private float _radius;
    [SerializeField] private HeroAbility _heroAbility;
    [SerializeField] private Sprite _heroSprite;
    [SerializeField] private Sprite _heroViewSprite;
    [SerializeField] private AudioClip _hitSound;
    [SerializeField] private ParticleSystem _damageParticles;

    public ParticleSystem DamageParticles => _damageParticles;

    public float BaseHealth => _baseHealth;

    public float BaseSpeed => _baseSpeed;

    public float BaseDamage => _baseDamage;

    public float Mass => _mass;

    public float Radius => _radius;

    public Sprite HeroSprite => _heroSprite;

    public Sprite HeroViewSprite => _heroViewSprite;

    public AudioClip HitSound => _hitSound;

    public HeroAbility HeroAbility => _heroAbility;
}