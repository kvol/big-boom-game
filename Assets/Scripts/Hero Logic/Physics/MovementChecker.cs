﻿using System;
using UnityEngine;


[RequireComponent(typeof(Rigidbody2D))]
public class MovementChecker : MonoBehaviour
{
    public event Action<MovementChecker, State> StateChanged;

    private State _currentState = State.Awake;
    private Rigidbody2D _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        State state = _rigidbody.IsAwake() ? State.Awake : State.Sleep;
        if (_currentState.Equals(state))
        {
            return;
        }
        else
        {
            _currentState = state;
            StateChanged?.Invoke(this, _currentState);
        }
    }


    public enum State
    {
        Sleep,
        Awake
    }
}