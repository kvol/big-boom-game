using System;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
public class HeroLauncher : MonoBehaviour
{
    public event Action HeroLaunched;

    private Rigidbody2D _rigidbody;
    private float? _launchingSpeed;

    public float? LaunchingSpeed
    {
        get => _launchingSpeed;
        set => _launchingSpeed = value;
    }

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
    }

    public void Launch(Vector2 aimDirection, float launchPowerModifier)
    {
        aimDirection = aimDirection.normalized;
        _rigidbody.velocity = aimDirection * launchPowerModifier * _launchingSpeed.GetValueOrDefault(0);
        HeroLaunched?.Invoke();
    }
}