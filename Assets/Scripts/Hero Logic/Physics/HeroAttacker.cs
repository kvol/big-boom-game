using System;
using Abilities;
using Abilities.Ally;
using Abilities.Enemy;
using Interfaces;
using UnityEngine;
using UnityEngine.Events;

//TODO:Разделить детект и атаку
[RequireComponent(typeof(Collider2D))]
public class HeroAttacker : MonoBehaviour
{
    private Hero.FightingSide _side;
    private float _damage;
    private bool _isAttacker;
    private AllyHeroAbility _allyHeroAbility;
    private EnemyHeroAbility _enemyHeroAbility;

    public event Action HeroAttacked;

    [SerializeField] private UnityEvent _heroCollided;

    public bool IsAttacker
    {
        get => _isAttacker;
        set => _isAttacker = value;
    }

    public bool IsLaunched { get; set; }

    public void Initialize(float damage, Hero.FightingSide side, HeroAbility ability)
    {
        _side = side;
        _damage = damage;

        if (ability is EnemyHeroAbility heroAbility)
        {
            _enemyHeroAbility = heroAbility;
        }
        else
        {
            _allyHeroAbility = ability as AllyHeroAbility;
        }
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        _heroCollided.Invoke();
        if (!other.gameObject.TryGetComponent(out IAttackable hero)) return;
        if (!_isAttacker) return;
        if (IsAlly(hero))
        {
            if (IsLaunched) _allyHeroAbility?.Apply(hero);
        }
        else
        {
            hero.ReceivedDamageMultiplier += _damage;
            HeroAttacked?.Invoke();
            if (IsLaunched) _enemyHeroAbility?.Apply(hero);
        }
    }

    private bool IsAlly(IAttackable hero)
    {
        return hero.Side.Equals(_side);
    }
}