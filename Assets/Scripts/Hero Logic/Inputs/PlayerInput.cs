using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace Inputs
{
    [RequireComponent(typeof(IInputHandler), typeof(Collider2D))]
    public class PlayerInput : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        private IInputHandler _heroInputHandler;
        private Camera _mainCamera;

        private void Start()
        {
            _mainCamera = Camera.main;
            _heroInputHandler = GetComponent<IInputHandler>();
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
            _heroInputHandler.OnBeginInput(CalculateAimDirection(eventData.position));
        }

        public void OnDrag(PointerEventData eventData)
        {
            _heroInputHandler.OnInput(CalculateAimDirection(eventData.position));
        }

        public void OnEndDrag(PointerEventData eventData)
        {
            _heroInputHandler.OnEndInput(CalculateAimDirection(eventData.position));
        }

        private Vector2 CalculateAimDirection(Vector2 dragPoint)
        {
            dragPoint = _mainCamera.ScreenToWorldPoint(dragPoint);
            Vector2 aimDirection = ((Vector2) transform.position - dragPoint);
            return aimDirection;
        }
    }
}