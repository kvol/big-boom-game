﻿using UnityEngine;

namespace Inputs
{
    public interface IInputHandler
    {
        void OnBeginInput(Vector2 inputPoint);
        void OnInput(Vector2 inputPoint);
        void OnEndInput(Vector2 inputPoint);
    }
}