﻿using Interfaces;

namespace Buffs
{
    public class ReduceSpeedBuff : Buff
    {
        private float _massIncreasePercent;

        public ReduceSpeedBuff(float massIncreasePercent, int ticksToRemove)
        {
            _massIncreasePercent = massIncreasePercent;
            TicksToRemove = ticksToRemove;
        }

        protected override void RemoveBuff()
        {
            HeroStat.Mass /= (1 + _massIncreasePercent / 100);
        }

        public override void Apply(IHeroStats heroToApply)
        {
            HeroStat = heroToApply;
            heroToApply.Mass *= (1 + _massIncreasePercent / 100);
        }
    }
}