﻿using Interfaces;
using UnityEngine;

namespace Buffs
{
    public abstract class Buff
    {
        protected int TicksToRemove;
        protected IHeroStats HeroStat;
        private bool _isExpired = false;

        public bool IsExpired
        {
            get
            {
                if (_isExpired)
                {
                    RemoveBuff();
                }

                return _isExpired;
            }
            private set => _isExpired = value;
        }

        protected abstract void RemoveBuff();


        public abstract void Apply(IHeroStats heroToApply);

        public virtual void Tick()
        {
            TicksToRemove--;
            if (TicksToRemove < 1)
                IsExpired = true;
        }
    }
}