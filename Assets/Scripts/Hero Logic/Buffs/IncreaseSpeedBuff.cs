﻿using Interfaces;

namespace Buffs
{
    public class IncreaseSpeedBuff : Buff
    {
        private float _massDecreasePercent;

        public IncreaseSpeedBuff(float massDecreasePercent, int ticksToRemove)
        {
            _massDecreasePercent = massDecreasePercent;
            TicksToRemove = ticksToRemove;
        }

        protected override void RemoveBuff()
        {
            HeroStat.Mass *= (1 + _massDecreasePercent / 100);
        }

        public override void Apply(IHeroStats heroToApply)
        {
            HeroStat = heroToApply;
            heroToApply.Mass /= (1 + _massDecreasePercent / 100);
        }
    }
}