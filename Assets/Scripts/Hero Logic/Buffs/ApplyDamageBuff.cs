﻿using Interfaces;

namespace Buffs
{
    public sealed class ApplyDamageBuff : Buff
    {
        private float _damageReduceAmount;
        private float _zoneDamage;

        public ApplyDamageBuff(float damageReduceAmount, float zoneDamage)
        {
            _damageReduceAmount = damageReduceAmount;
            _zoneDamage = zoneDamage;
            TicksToRemove = 0;
            Tick();
        }

        protected override void RemoveBuff()
        {
        }

        public override void Apply(IHeroStats heroToApply)
        {
            HeroStat = heroToApply;
            HeroStat.ReceivedDamageMultiplier -= _damageReduceAmount;
            ((Hero) HeroStat).ApplyDamage(_zoneDamage);
        }
    }
}