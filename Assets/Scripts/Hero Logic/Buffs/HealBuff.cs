﻿using Interfaces;
using UnityEngine;

namespace Buffs
{
    public sealed class HealBuff : Buff
    {
        private readonly float _healAmount;

        public HealBuff(float healAmount)
        {
            _healAmount = healAmount;
            TicksToRemove = 0;
            Tick();
        }

        protected override void RemoveBuff()
        {
        }

        public override void Apply(IHeroStats heroToApply)
        {
            heroToApply.CurrentHealth = Mathf.Clamp(heroToApply.CurrentHealth + _healAmount, 0, heroToApply.MAXHealth);
        }
    }
}