using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Interfaces;
using UnityEngine;

public class HeroesActionsListener : MonoBehaviour
{
    [SerializeField] private ParticleSystem _deathVFXPrefab;

    public event Action PlayerLaunchedHero;

    public event Action<HeroesActionsListener> AllHeroesFellAsleep;

    public readonly Dictionary<IHeroActions, MovementChecker.State> _heroesStates =
        new Dictionary<IHeroActions, MovementChecker.State>();

    private Coroutine _sleepingCheckRoutine;

    public void SubscribeToHeroEvents()
    {
        foreach (var hero in GetComponentsInChildren<IHeroActions>())
        {
            hero.Launched += OnHeroLaunched;
            hero.ChangedMovingState += OnHeroChangedMovingState;
            hero.Died += OnHeroDied;
        }
    }

    private void OnHeroDied(IHeroActions hero, Vector3 position)
    {
        _heroesStates.Remove(hero);
        CreateDeathVFX(position);
    }

    private void CreateDeathVFX(Vector3 position)
    {
        var vfxInstance = Instantiate(_deathVFXPrefab, position, Quaternion.identity, transform);
        DOVirtual.DelayedCall(5, () => Destroy(vfxInstance.gameObject));
    }

    private void UnsubscribeFromHeroEvents()
    {
        foreach (var hero in GetComponentsInChildren<IHeroActions>())
        {
            hero.Launched -= OnHeroLaunched;
            hero.ChangedMovingState -= OnHeroChangedMovingState;
            hero.Died -= OnHeroDied;
        }
    }

    private void OnHeroChangedMovingState(IHeroActions movable, MovementChecker.State state)
    {
        _heroesStates[movable] = state;

        _sleepingCheckRoutine ??= StartCoroutine(CheckSleepingState());
    }

    private IEnumerator CheckSleepingState()
    {
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        if (IsAllHeroesSleeping())
        {
            AllHeroesFellAsleep?.Invoke(this);
        }

        _sleepingCheckRoutine = null;
    }

    private void OnDestroy()
    {
        UnsubscribeFromHeroEvents();
    }


    private void OnHeroLaunched(IHeroActions heroActions)
    {
        PlayerLaunchedHero?.Invoke();
    }

    public bool IsAllHeroesSleeping()
    {
        return _heroesStates.All(pair => pair.Value == MovementChecker.State.Sleep);
    }
}