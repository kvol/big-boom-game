﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utilities;
using Utilities.Enums;


public class SceneController : DontDestroy<SceneController>
{
    private const string BaseLevelName = "Level ";
    private const string MenuSceneName = "Main menu";

    private int _levelNumber;
    private int _lastLevelNumber;

    private SceneType _currentSceneType;


    private new void Awake()
    {
        base.Awake();
        _levelNumber = 1;
        _currentSceneType = SceneType.Menu;
        _lastLevelNumber = SceneManager.sceneCountInBuildSettings - 1;
    }

    [ContextMenu("Next")]
    private void NextLevel()
    {
        ChangeScene(LoadSceneType.NextLevel);
    }

    public void ChangeScene(LoadSceneType sceneType)
    {
        string sceneToLoadName;
        switch (sceneType)
        {
            case LoadSceneType.NextLevel:
                if (_levelNumber < _lastLevelNumber)
                    _levelNumber++;
                sceneToLoadName = BaseLevelName + _levelNumber;
                _currentSceneType = SceneType.Level;
                break;
            case LoadSceneType.MainMenu:
                sceneToLoadName = MenuSceneName;
                _currentSceneType = SceneType.Menu;
                break;
            case LoadSceneType.Restart:
            case LoadSceneType.CurrentLevel:
                sceneToLoadName = BaseLevelName + _levelNumber;
                _currentSceneType = SceneType.Level;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(sceneType), sceneType, null);
        }

        SceneManager.LoadScene(sceneToLoadName);
    }
}