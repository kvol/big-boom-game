﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Inputs;
using Interfaces;
using TurnManagers;
using TurnManagers.Stages;
using UnityEditor.UIElements;
using UnityEngine;

namespace AI
{
    public class AIController : MonoBehaviour
    {
        [SerializeField] private AnimationCurve aimTimeCurve;


        private List<Hero> _enemyHeroes;
        private List<Hero> _friendlyHeroes;

        public void Initialize(List<Hero> enemyHeroes, List<Hero> friendlyHeroes)
        {
            _enemyHeroes = enemyHeroes;
            _friendlyHeroes = friendlyHeroes;

            foreach (var friendlyHero in _friendlyHeroes)
            {
                friendlyHero.ReadyToLaunch += OnReadyToLaunch;
            }
        }

        private void OnReadyToLaunch(IHeroActions iHero)
        {
            Hero hero = iHero as Hero;
            if (hero == null)
                return;
            Vector2 currentHeroPosition = hero.transform.position;
            IInputHandler heroInput = hero;
            StartCoroutine(Aim(currentHeroPosition, heroInput));
        }

        private IEnumerator Aim(Vector2 currentHeroPosition, IInputHandler heroInput)
        {
            Vector2 aimPoint = FindClosestEnemyPosition(currentHeroPosition);
            Vector2 endAimDirection = (aimPoint - currentHeroPosition).normalized;
            Vector2 startAimDirection = Random.insideUnitCircle.normalized;
            float startShotPower = Random.Range(0.1f, PowerBar.MaxLenghtOfDrag);
            float endShotPower = Random.Range(0.1f, PowerBar.MaxLenghtOfDrag);
            float aimTime = aimTimeCurve.Evaluate(Random.value) * LaunchStage.AimTime / 4;
            float time = 0;

            heroInput.OnBeginInput(Vector2.zero);

            while (aimTime > time)
            {
                float normalizedTime = time / aimTime;
                Vector2 aimDirection = Vector2.Lerp(startAimDirection, endAimDirection, normalizedTime);
                float shotPower = Mathf.Lerp(startShotPower, endShotPower, normalizedTime);
                heroInput.OnInput(aimDirection * shotPower);
                yield return null;
                time += Time.deltaTime;
            }

            heroInput.OnEndInput(endAimDirection * endShotPower);
        }

        private Vector2 FindClosestEnemyPosition(Vector2 currentHeroPosition)
        {
            for (int i = _enemyHeroes.Count - 1; i >= 0; i--)
            {
                if (_enemyHeroes[i] == null)
                    _enemyHeroes.RemoveAt(i);
            }

            return _enemyHeroes.OrderBy(hero => (currentHeroPosition - (Vector2) hero.transform.position).magnitude)
                .First()
                .transform.position;
        }
    }
}