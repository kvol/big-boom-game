using System;
using System.Collections.Generic;
using TMPro;
using TurnManagers.Stages;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace TurnManagers
{
    [RequireComponent(typeof(SpawnStage), typeof(ActionsOfHeroesStage), typeof(LaunchStage))]
    [RequireComponent(typeof(DamageStage), typeof(EndTurnStage))]
    public class TurnManager : MonoBehaviour
    {
        private HeroesActionsListener _heroesBlueActionsListener;
        private HeroesActionsListener _heroesRedActionsListener;

        public event Action<List<Queue<Hero>>> HeroesQueueCreated;

        private Stage _currentStage;

        private SpawnStage _spawnStage;
        private LaunchStage _launchStage;
        private DamageStage _damageStage;
        private EndTurnStage _endTurn;
        private ActionsOfHeroesStage _actionsOfHeroesStage;

        private HeroesStateController _heroesStateController;
        private AttackerSideController _attackerSideController;
        private int _turnNumber;
        private List<List<Hero>> _spawnedHeroes;

        private bool _isRedSideAI;
        private Hero.FightingSide _currentAttackingSide;

        public void Initialize(bool isRedSideAI, HeroesActionsListener heroesActionsListenerBlue,
            HeroesActionsListener heroesActionsListenerRed, Action<bool> onGameEnded)
        {
            _isRedSideAI = isRedSideAI;
            _heroesBlueActionsListener = heroesActionsListenerBlue;
            _heroesRedActionsListener = heroesActionsListenerRed;

            GetRequiredComponents();
            SubscribeAllEndStageEvents();
            _endTurn.MatchEnded += onGameEnded;
        }


        private void InitializeStages()
        {
            _launchStage.Initialize(_heroesBlueActionsListener, _heroesRedActionsListener, _spawnedHeroes);
            _actionsOfHeroesStage.Initialize(_heroesBlueActionsListener, _heroesRedActionsListener);
            _endTurn.Initialize(_spawnedHeroes);
        }

        private void SubscribeAllEndStageEvents()
        {
            _launchStage.StageEnded += OnLaunchStageEnded;
            _endTurn.StageEnded += OnEndTurnStageEnded;
            _actionsOfHeroesStage.StageEnded += OnActionsOfHeroesEnded;
        }

        private void GetRequiredComponents()
        {
            _spawnStage = GetComponent<SpawnStage>();
            _launchStage = GetComponent<LaunchStage>();
            _damageStage = GetComponent<DamageStage>();
            _endTurn = GetComponent<EndTurnStage>();
            _actionsOfHeroesStage = GetComponent<ActionsOfHeroesStage>();
        }

        private void ChangeStage(Stage newStage)
        {
            while (true)
            {
                _currentStage = newStage;
                switch (_currentStage)
                {
                    case Stage.Spawning:
                        _spawnedHeroes = _spawnStage.SpawnAllHeroes(_isRedSideAI);
                        _heroesBlueActionsListener.SubscribeToHeroEvents();
                        _heroesRedActionsListener.SubscribeToHeroEvents();
                        InitializeStages();
                        HeroesQueueCreated?.Invoke(_launchStage.LaunchQueues);
                        _attackerSideController = new AttackerSideController(Hero.FightingSide.Blue);
                        _heroesStateController = new HeroesStateController(_spawnedHeroes);
                        newStage = Stage.Launching;
                        continue;
                    case Stage.Launching:
                        _currentAttackingSide = _attackerSideController.GetCurrentSideToLaunchAndInvert();
                        _launchStage.LaunchHero(_currentAttackingSide);
                        break;
                    case Stage.ActionsOfHeroes:
                        _heroesStateController.SetSideHeroesAsAttackers(_currentAttackingSide);
                        _actionsOfHeroesStage.WaitAllHeroes();
                        break;
                    case Stage.ApplyingDamage:
                        _damageStage.StartDamageApplying();
                        _heroesStateController.SetSideHeroesAsDefenders(_currentAttackingSide);
                        newStage = Stage.TurnEnd;
                        continue;
                    case Stage.TurnEnd:
                        _endTurn.EndTurn();
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                break;
            }
        }

        public void StartBattle()
        {
            _turnNumber = 1;
            ChangeStage(Stage.Spawning);
        }

        private void OnActionsOfHeroesEnded()
        {
            ChangeStage(Stage.ApplyingDamage);
        }

        private void OnLaunchStageEnded()
        {
            ChangeStage(Stage.ActionsOfHeroes);
        }

        private void OnEndTurnStageEnded()
        {
            _turnNumber++;
            ChangeStage(Stage.Launching);
        }


        private enum Stage
        {
            Spawning,
            ActionsOfHeroes,
            Launching,
            ApplyingDamage,
            TurnEnd
        }
    }
}