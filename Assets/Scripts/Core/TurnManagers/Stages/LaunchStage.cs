using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;
using Utilities;

namespace TurnManagers.Stages
{
    public class LaunchStage : Stage
    {
        public const float AimTime = 30;

        private HeroesActionsListener _heroesBlueActionsListener;
        private HeroesActionsListener _heroesRedActionsListener;

        private List<List<Hero>> _spawnedHeroes;
        private readonly List<Queue<Hero>> _launchQueues = new List<Queue<Hero>>();

        private Hero _currentLaunchingHero;

        private bool _isLaunchEnded;

        private Tween _timer;

        public List<Queue<Hero>> LaunchQueues => new List<Queue<Hero>>(new[]
            {new Queue<Hero>(_launchQueues[0]), new Queue<Hero>(_launchQueues[1])});

        public void Initialize(HeroesActionsListener heroesActionsListenerBlue,
            HeroesActionsListener heroesActionsListenerRed, List<List<Hero>> spawnedHeroes)
        {
            _heroesBlueActionsListener = heroesActionsListenerBlue;
            _heroesRedActionsListener = heroesActionsListenerRed;

            _heroesBlueActionsListener.PlayerLaunchedHero += OnHeroesLaunchedHero;
            _heroesRedActionsListener.PlayerLaunchedHero += OnHeroesLaunchedHero;

            _spawnedHeroes = spawnedHeroes;

            MakeLaunchQueue();
        }

        private void MakeLaunchQueue()
        {
            _launchQueues.Add(new Queue<Hero>());
            _launchQueues.Add(new Queue<Hero>());
            for (int i = 0; i < SpawnStage.MAXHeroesOnSide; i++)
            {
                const int blue = (int) Hero.FightingSide.Blue;
                const int red = (int) Hero.FightingSide.Red;
                _launchQueues[blue].Enqueue(_spawnedHeroes[blue][i]);
                _launchQueues[red].Enqueue(_spawnedHeroes[red][i]);
            }
        }

        private Hero GetHeroFromQueueAndEnqueue(Hero.FightingSide launchSide)
        {
            Hero hero = null;
            int side = (int) launchSide;

            hero = GetFirstNotDestroyedHero(side);

            return hero;
        }

        private IEnumerator LaunchNextHero(Hero.FightingSide currentSide)
        {
            yield return null;
            Hero heroToLaunch = GetHeroFromQueueAndEnqueue(currentSide);
            yield return StartHeroLaunching(heroToLaunch);
            StageEnded?.Invoke();
        }


        private IEnumerator StartHeroLaunching(Hero heroToLaunch)
        {
            _currentLaunchingHero = heroToLaunch;
            _isLaunchEnded = false;
            heroToLaunch.IsLaunchable = true;

            _timer = DOVirtual.DelayedCall(AimTime, HeroLaunchEnded);
            return new WaitUntil((() => _isLaunchEnded));
        }

        private void OnHeroesLaunchedHero()
        {
            HeroLaunchEnded();
        }

        private void HeroLaunchEnded()
        {
            _timer.Kill();
            _currentLaunchingHero.IsLaunchable = false;
            _isLaunchEnded = true;
        }


        private Hero GetFirstNotDestroyedHero(int side)
        {
            return _launchQueues[side].ReEnqueue(item => item != null);
        }

        public void LaunchHero(Hero.FightingSide launchSide)
        {
            StartCoroutine(LaunchNextHero(launchSide));
        }
    }
}