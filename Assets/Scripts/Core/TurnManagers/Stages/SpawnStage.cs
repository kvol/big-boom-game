using System;
using System.Collections.Generic;
using Inputs;
using UnityEngine;

namespace TurnManagers.Stages
{
    public class SpawnStage : Stage
    {
        public const int MAXHeroesOnSide = 4;
        [SerializeField] private List<Hero> _heroesToSpawnBlue = new List<Hero>(MAXHeroesOnSide);
        [SerializeField] private List<Hero> _heroesToSpawnRed = new List<Hero>(MAXHeroesOnSide);

        [SerializeField] private GameObject _heroesSpotsRootBlue;
        [SerializeField] private GameObject _heroesSpotsRootRed;

        [SerializeField] private Transform _rootOfHeroesBlue;
        [SerializeField] private Transform _rootOfHeroesRed;

        private List<HeroSpot> _spotsToSpawnBlue = new List<HeroSpot>(MAXHeroesOnSide);
        private List<HeroSpot> _spotsToSpawnRed = new List<HeroSpot>(MAXHeroesOnSide);

        private List<List<Hero>> _spawnedHeroes;

        public List<List<Hero>> SpawnedHeroes
        {
            get => _spawnedHeroes;
            private set => _spawnedHeroes = value;
        }

        private void Awake()
        {
            _spotsToSpawnBlue = new List<HeroSpot>(_heroesSpotsRootBlue.GetComponentsInChildren<HeroSpot>());
            _spotsToSpawnRed = new List<HeroSpot>(_heroesSpotsRootRed.GetComponentsInChildren<HeroSpot>());
            Debug.Assert(_heroesToSpawnBlue.Count == _spotsToSpawnBlue.Count,
                "Spawn spots of Blue not equal to heroes count");
            Debug.Assert(_heroesToSpawnRed.Count == _spotsToSpawnRed.Count,
                "Spawn spots of Red not equal to heroes count");
        }

        public List<List<Hero>> SpawnAllHeroes(bool isRedSideAI)
        {
            _spawnedHeroes = new List<List<Hero>>
            {
                new List<Hero>(),
                new List<Hero>()
            };
            SpawnSideHeroes(_heroesToSpawnBlue, _spotsToSpawnBlue, _rootOfHeroesBlue, Hero.FightingSide.Blue, true);
            SpawnSideHeroes(_heroesToSpawnRed, _spotsToSpawnRed, _rootOfHeroesRed, Hero.FightingSide.Red, !isRedSideAI);
            return _spawnedHeroes;
        }

        private void SpawnSideHeroes(List<Hero> heroesToSpawn, List<HeroSpot> spotsToSpawn, Transform rootOfHeroes,
            Hero.FightingSide side, bool isSidePlayable)
        {
            for (int i = 0; i < MAXHeroesOnSide; i++)
            {
                Hero spawnedHero = Instantiate(heroesToSpawn[i], spotsToSpawn[i].transform.position,
                    Quaternion.identity, rootOfHeroes);
                spawnedHero.name = $"Hero{i} {side}";
                spawnedHero.Side = side;
                if (isSidePlayable)
                {
                    spawnedHero.gameObject.AddComponent(typeof(PlayerInput));
                }

                _spawnedHeroes[(int) side].Add(spawnedHero);
            }
        }
    }
}