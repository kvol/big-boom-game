using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Interfaces;
using UnityEngine;

//TODO: Make damage modificator per turn!
namespace TurnManagers.Stages
{
    public class DamageStage : Stage
    {
        [SerializeField] private GameObject _damageZonesRoot;


        private readonly List<IDamageZone> _damageZones = new List<IDamageZone>();
        private Dictionary<IDamageable, float> _heroesToApplyDamage = new Dictionary<IDamageable, float>();


        private void Start()
        {
            foreach (var damageZone in _damageZonesRoot.GetComponentsInChildren<IDamageZone>())
            {
                _damageZones.Add(damageZone);
            }
        }

        public void StartDamageApplying()
        {
            GetAllPlayersInsideDamageZones();
            ApplyDamageToAllHeroes();
        }

        private void GetAllPlayersInsideDamageZones()
        {
            foreach (var damageZone in _damageZones)
            {
                _heroesToApplyDamage = _heroesToApplyDamage.Concat(damageZone.GetHeroesToApplyDamage()
                    .Where(pair =>
                    {
                        if (_heroesToApplyDamage.ContainsKey(pair.Key))
                        {
                            var value = _heroesToApplyDamage[pair.Key];
                            _heroesToApplyDamage[pair.Key] = Mathf.Max(pair.Value, value);
                            return false;
                        }

                        return true;
                    })).ToDictionary(x => x.Key, y => y.Value);
            }
        }

        private void ApplyDamageToAllHeroes()
        {
            foreach (var heroToApply in _heroesToApplyDamage)
            {
                heroToApply.Key.ApplyDamage(heroToApply.Value);
            }

            _heroesToApplyDamage.Clear();
        }
    }
}