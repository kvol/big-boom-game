﻿using System;
using UnityEngine;

namespace TurnManagers.Stages
{
    public abstract class Stage : MonoBehaviour
    {
        public Action StageEnded;
    }
}