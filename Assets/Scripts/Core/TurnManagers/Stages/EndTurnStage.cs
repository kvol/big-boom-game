﻿using System;
using System.Collections.Generic;
using DG.Tweening;

namespace TurnManagers.Stages
{
    public class EndTurnStage : Stage
    {
        public event Action<bool> MatchEnded;

        private bool _isSomeoneWon = false;

        private List<List<Hero>> _spawnedHeroes;

        public void Initialize(List<List<Hero>> spawnedHeroes)
        {
            _spawnedHeroes = spawnedHeroes;
        }

        public void EndTurn()
        {
            DOVirtual.DelayedCall(0.1f, CheckWin);
        }

        private void CheckWin()
        {
            CheckWinConditions(Hero.FightingSide.Red);
            if (_isSomeoneWon)
            {
                MatchEnded?.Invoke(true);
            }
            else
            {
                CheckWinConditions(Hero.FightingSide.Blue);
                if (_isSomeoneWon)
                {
                    MatchEnded?.Invoke(false);
                }
                else
                {
                    StageEnded?.Invoke();
                }
            }
        }

        private void CheckWinConditions(Hero.FightingSide side)
        {
            int counter = SpawnStage.MAXHeroesOnSide;
            foreach (var hero in _spawnedHeroes[(int) side])
            {
                if (hero == null)
                {
                    counter--;
                }
            }

            if (counter == 0)
            {
                _isSomeoneWon = true;
            }
        }
    }
}