﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using UnityEngine;

namespace TurnManagers.Stages
{
    public class ActionsOfHeroesStage : Stage
    {
        private HeroesActionsListener _heroesBlueActionsListener;
        private HeroesActionsListener _heroesRedActionsListener;

        private readonly Dictionary<HeroesActionsListener, bool> _sideIsSleeping =
            new Dictionary<HeroesActionsListener, bool>();

        public void Initialize(HeroesActionsListener heroesActionsListenerBlue,
            HeroesActionsListener heroesActionsListenerRed)
        {
            _heroesBlueActionsListener = heroesActionsListenerBlue;
            _heroesRedActionsListener = heroesActionsListenerRed;
        }

        private IEnumerator WaitUntilAllHeroesStopped()
        {
            ResetAllSleepingHeroes();
            yield return new WaitForFixedUpdate();
            UpdateIsHeroSleeping();

            if (CheckAllHeroesSleeping())
            {
                StageEnded?.Invoke();
                yield break;
            }

            _heroesBlueActionsListener.AllHeroesFellAsleep += OnHeroesStopped;
            _heroesRedActionsListener.AllHeroesFellAsleep += OnHeroesStopped;
        }

        private void ResetAllSleepingHeroes()
        {
            _sideIsSleeping[_heroesBlueActionsListener] = false;
            _sideIsSleeping[_heroesRedActionsListener] = false;
        }

        private bool CheckAllHeroesSleeping()
        {
            return _sideIsSleeping.All(side => side.Value);
        }

        private void UpdateIsHeroSleeping()
        {
            _sideIsSleeping[_heroesBlueActionsListener] = _heroesBlueActionsListener.IsAllHeroesSleeping();
            _sideIsSleeping[_heroesRedActionsListener] = _heroesRedActionsListener.IsAllHeroesSleeping();
        }

        private void OnHeroesStopped(HeroesActionsListener heroesActionsListener)
        {
            _sideIsSleeping[heroesActionsListener] = true;
            if (CheckAllHeroesSleeping())
            {
                _heroesBlueActionsListener.AllHeroesFellAsleep -= OnHeroesStopped;
                _heroesRedActionsListener.AllHeroesFellAsleep -= OnHeroesStopped;
                StageEnded?.Invoke();
            }
        }

        public void WaitAllHeroes()
        {
            StartCoroutine(WaitUntilAllHeroesStopped());
        }
    }
}