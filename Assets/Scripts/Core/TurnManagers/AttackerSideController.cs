﻿namespace TurnManagers
{
    public class AttackerSideController
    {
        public AttackerSideController(Hero.FightingSide currentAttackerSide)
        {
            _currentAttackerSide = currentAttackerSide;
        }

        private Hero.FightingSide _currentAttackerSide;

        public Hero.FightingSide CurrentAttackerSide => _currentAttackerSide;

        public Hero.FightingSide GetCurrentSideToLaunchAndInvert()
        {
            _currentAttackerSide = _currentAttackerSide == Hero.FightingSide.Blue
                ? Hero.FightingSide.Red
                : Hero.FightingSide.Blue;
            return _currentAttackerSide;
        }
    }
}