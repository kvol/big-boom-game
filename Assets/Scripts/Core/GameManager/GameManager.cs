﻿using System;
using System.Collections.Generic;
using AI;
using Interfaces;
using TurnManagers;
using TurnManagers.Stages;
using UI;
using UnityEngine;
using Utilities.Enums;

namespace GameManager
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private HeroesActionsListener _heroesBlueActionsListener;
        [SerializeField] private HeroesActionsListener _heroesRedActionsListener;


        [SerializeField] private TurnManager _turnManager;
        [SerializeField] private AIController _aIController;
        [SerializeField] private UIManager _uIManager;
        private readonly List<List<Hero>> _spawnedHeroes = new List<List<Hero>>() {new List<Hero>(), new List<Hero>()};

        private SceneController _sceneController;

        private void Start()
        {
            _sceneController = FindObjectOfType<SceneController>();
            OnButtonStartPressed();
        }

        private void OnButtonStartPressed()
        {
            _turnManager.Initialize(true, _heroesBlueActionsListener, _heroesRedActionsListener, OnGameEnded);
            _turnManager.HeroesQueueCreated += OnHeroesSpawned;
            _turnManager.StartBattle();
        }

        private void OnHeroesSpawned(List<Queue<Hero>> spawnedHeroes)
        {
            List<Queue<IHeroView>> heroesStatsQueues = new List<Queue<IHeroView>>
            {
                new Queue<IHeroView>(),
                new Queue<IHeroView>()
            };
            foreach (var hero in spawnedHeroes[(int) Hero.FightingSide.Blue])
            {
                _spawnedHeroes[(int) Hero.FightingSide.Blue].Add(hero);
            }

            foreach (var hero in spawnedHeroes[(int) Hero.FightingSide.Red])
            {
                _spawnedHeroes[(int) Hero.FightingSide.Red].Add(hero);
            }

            for (int i = 0; i < SpawnStage.MAXHeroesOnSide; i++)
            {
                heroesStatsQueues[(int) Hero.FightingSide.Blue]
                    .Enqueue(spawnedHeroes[(int) Hero.FightingSide.Blue].Dequeue());
                heroesStatsQueues[(int) Hero.FightingSide.Red]
                    .Enqueue(spawnedHeroes[(int) Hero.FightingSide.Red].Dequeue());
            }

            _aIController.Initialize(_spawnedHeroes[(int) Hero.FightingSide.Blue],
                _spawnedHeroes[(int) Hero.FightingSide.Red]);
            _uIManager.Initialize(heroesStatsQueues, ChangeSceneCallback);
        }

        private void OnGameEnded(bool isPlayerWon)
        {
            _uIManager.OpenWindow(isPlayerWon ? WindowType.Victory : WindowType.Lose);
        }

        private void ChangeSceneCallback(LoadSceneType loadSceneType)
        {
            _sceneController.ChangeScene(loadSceneType);
        }
    }
}